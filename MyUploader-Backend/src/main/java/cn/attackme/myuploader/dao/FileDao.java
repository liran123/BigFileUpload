package cn.attackme.myuploader.dao;

import cn.attackme.myuploader.model.File;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FileDao {
    /**
     * 通过主键获取一行数据
     *
     * @return
     */
    File getById(Long id);

    /**
     * 插入一行数据
     *
     * @param file
     * @return
     */
    int save(File file);

    /**
     * 更新一行数据
     *
     * @param file
     * @return
     */
    int update(File file);

    /**
     * 删除一行数据
     *
     * @param id
     * @return
     */
    int deleteById(Long id);

    /**
     * 根据一个或多个属性获取File
     *
     * @param file
     * @return
     */
    List<File> getByFile(File file);
}
