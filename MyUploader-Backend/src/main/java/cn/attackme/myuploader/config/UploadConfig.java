package cn.attackme.myuploader.config;

import cn.attackme.myuploader.utils.FileUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;

@Configuration
public class UploadConfig {

    public static String path;

    @Value("${upload.path}")
    public void setPath(String path) {
        UploadConfig.path = path;
    }

    @PostConstruct
    public void init() {
        FileUtils.beforeCreateDirs(UploadConfig.path);
    }
}
